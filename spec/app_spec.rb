require File.dirname(__FILE__) + '/spec_helper'

describe "Doomboard" do
  include Rack::Test::Methods
  
  def app
    @app ||= Sinatra::Application
  end

  it "should require Redis" do 
    r = Redis.new
    r.should be_a(Redis)
  end
end

describe "Doomboard", "GET /" do
  include Rack::Test::Methods
  
  before :each do
    REDIS.flush_all
    10.times { post "/hook/#{TOKEN}", :payload => payload }
  end
  
  def app
    @app ||= Sinatra::Application
  end
  
  it "should fetch active projects" do
    get '/'
    
    body = Nokogiri::HTML(last_response.body)
    body.css("#left-items li").size.should == 10
    
    last_response.should be_ok
  end
  
  it "should not fetch private projects" do
    post "/hook/#{TOKEN}", :payload => payload(:private => true) 
    
    get '/'
    
    body = Nokogiri::HTML(last_response.body)
    body.css("#left-items li").size.should == 10
    
    last_response.should be_ok
  end
  
  it "should fetch private projects if /private is added to the url" do
    post "/hook/#{TOKEN}", :payload => payload(:private => true) 
    
    get '/private', {}, {'HTTP_AUTHORIZATION'=> encode_credentials('pablo', 'picasso')}
        
    body = Nokogiri::HTML(last_response.body)
    body.css("#left-items li").size.should == 11
    
    last_response.should be_ok
  end
  
end

describe "Doomboard", "GET /projects" do
  include Rack::Test::Methods
  
  before :all do
    REDIS.flush_all
    10.times { post "/hook/#{TOKEN}", :payload => payload }
  end

  before :each do
    get '/projects'
    
    @body = Nokogiri::HTML(last_response.body)
  end
  
  def app
    @app ||= Sinatra::Application
  end
  
  it "should fetch active projects" do
    @body.css("#left-items li").size.should == 10
    last_response.should be_ok
  end
  
  it "should fetch projects ordered by name" do
    projects = @body.css("#left-items li").map {|e| e.content.split[0]}
    projects.should == projects.sort_by {|p| p}
  end  
  
  it "should query the continuous integration server for project status" do
    j = StringIO.new
    k = '{}'
    Kernel.should_receive(:open).with(CONTINOUS_INTEGRATION_URL).and_return(j)
    j.should_receive(:read).and_return(k)
    JSON.should_receive(:parse).with(k).and_return({"projects" => []})
    get '/projects'
  end
end

describe "Doomboard", "GET /commits/latest" do
  include Rack::Test::Methods
  
  before :all do
    REDIS.flush_all
    10.times { post "/hook/#{TOKEN}", :payload => payload }
  end

  before :each do
    get '/commits/latest'
  end
  
  def app
    @app ||= Sinatra::Application
  end
  
  it "should return json" do
    last_response.content_type.should == 'application/json'
  end
  
  it "should return the latest message" do
    my_payload = payload
    post "/hook/#{TOKEN}", :payload => payload
    get '/commits/latest'
    
    parsed_response = JSON.parse(last_response.body)
    parsed_payload = JSON.parse(my_payload)
    
    parsed_response["message"] == parsed_payload["commits"].last["message"]
    parsed_response["gravatar"] == Gravatar.url(parsed_payload["commits"].last["author"]["email"])
  end
end

describe "Doomboard", "GET /leaderboard/commits/*" do
  include Rack::Test::Methods
  
  before :all do
    REDIS.flush_all
    10.times { post "/hook/#{TOKEN}", :payload => payload }
  end
  
  def app
    @app ||= Sinatra::Application
  end
  
  it "should return the correct title (weekly)" do
    get "/leaderboard/commits/weekly"
    last_response.body.should match(/This week top committers/)
  end

  it "should return the correct title (daily)" do
    get "/leaderboard/commits/today"
    last_response.body.should match(/Top committers of the day/)
  end

  it "should return the correct title (all-times)" do
    get "/leaderboard/commits/all-times"
    last_response.body.should match(/Top all-time committers/)
  end
  
  it "should return 10 commiters" do
    20.times { post "/hook/#{TOKEN}", :payload => payload }
    get "/leaderboard/commits/all-times"
    body = Nokogiri::HTML(last_response.body)
    body.css(".user").size.should == 10
  end
  
  it "should call the correct REDIS key (weekly)" do
    leaderboard = []
    REDIS.should_receive(:keys).with("uid:*:weekly:#{Time.now.beginning_of_week.yday}").and_return(leaderboard)
    get "/leaderboard/commits/weekly"
  end

  it "should call the correct REDIS key (today)" do
    leaderboard = []
    REDIS.should_receive(:keys).with("uid:*:daily:#{Time.now.yday}").and_return(leaderboard)
    get "/leaderboard/commits/today"
  end

  it "should call the correct REDIS key (all-times)" do
    leaderboard = []
    REDIS.should_receive(:keys).with("uid:*:commits").and_return(leaderboard)
    get "/leaderboard/commits/all-times"
  end
  
  it "should sort by commits" do
    20.times { post "/hook/#{TOKEN}", :payload => payload(:author_email => "intinig@gmail.com") }
    get "/leaderboard/commits/all-times"
    body = Nokogiri::HTML(last_response.body)
    users = body.css(".user").map {|e| e.css("span.count").first.content}
    users.should == users.sort.reverse
  end
end