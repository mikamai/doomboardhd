(function($) {
  $.updatingFlyingDude = function() {
    $.getJSON('/commits/latest', function(object) {
      if (object) {
        $('#flying-dude .gravatar').attr('src', object.gravatar)
        $('#flying-dude .message').text(object.message)
      }
      window.setTimeout('jQuery.updatingFlyingDude()', 20000);
    });    
  }
  
  $(function() {
    $.updatingFlyingDude();
  });
})(jQuery)