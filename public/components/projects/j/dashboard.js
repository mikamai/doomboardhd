(function($) {
  $.updateProjectsDashboard = function() {
    if (fetchPrivate) {
      url = "/projects/private"
    } else {
      url = "/projects"
    }
    $.get(url, function(data) {
      $('#left-items').replaceWith(data)
    });
  }

  $(document).ready(function(){
    setInterval("jQuery.updateProjectsDashboard()", 60000 );
  });
  
})(jQuery)