require File.dirname(__FILE__) + '/spec_helper'

describe "Doomboard", "POST /hook/TOKEN" do
  include Rack::Test::Methods

  def app
    @app ||= Sinatra::Application
  end

  before :each do 
    REDIS.flush_all
    p = payload
    @data = JSON.parse(p)
    post "/hook/#{TOKEN}", :payload => p
  end
  
  it "should create a project" do
    pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
    pid.should_not be_nil
    REDIS.get("pid:#{pid}:name").should == @data["repository"]["name"]
  end
 
  it "should set the active status for a project" do
    pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
    REDIS.get("apid:#{pid}").should == pid
    REDIS.ttl("apid:#{pid}").should > 13*24*60*60
  end

  it "should try and get an existing project" do
    post "/hook/#{TOKEN}", :payload => @data.to_json
    
    REDIS.get("purl:#{@data["repository"]["url"]}:pid").should == "1"
  end
  
  it "should set project attributes" do
     pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
     REDIS.get("pid:#{pid}:private").should == @data["repository"]["private"].to_s
     REDIS.get("pid:#{pid}:name").should == @data["repository"]["name"].to_s
     REDIS.get("pid:#{pid}:min_url").should == @data["repository"]["url"].split("/").last.to_s
   end

   it "should create authors" do
     uid = REDIS.get("email:#{@data["commits"][0]["author"]["email"]}:uid")
     uid.should_not be_nil
     REDIS.get("uid:#{uid}:email").should == @data["commits"][0]["author"]["email"]
   end

   it "should set author name" do
     uid = REDIS.get("email:#{@data["commits"][0]["author"]["email"]}:uid")
     REDIS.get("uid:#{uid}:name").should == @data["commits"][0]["author"]["name"]
   end

   it "should set author gravatar" do
     uid = REDIS.get("email:#{@data["commits"][0]["author"]["email"]}:uid")
     REDIS.get("uid:#{uid}:gravatar").should == Gravatar.url(@data["commits"][0]["author"]["email"])
   end

   it "should try and get an existing author" do
     post "/hook/#{TOKEN}", :payload => payload

     REDIS.get("email:#{@data["commits"][0]["author"]["email"]}:uid").should == "1"
   end

   it "should add authors to projects" do
     pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
     REDIS.scard("pid:#{pid}:authors").should == 1
     REDIS.get("auid:1").should == "1"
   end
   
   it "should set latest committer for projects" do
     pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
     REDIS.get("pid:#{pid}:latest:gravatar").should == Gravatar.url(@data["commits"][0]["author"]["email"])
   end

   it "should store commit ids in project" do
     pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
     REDIS.scard("pid:#{pid}:commits").should == 1
     REDIS.srandmember("pid:#{pid}:commits").should == @data["commits"][0]["id"]
   end

   it "should expire authors more than two weeks old" do
     pid = REDIS.get("purl:#{@data["repository"]["url"]}:pid")
     uid = REDIS.srandmember("pid:#{pid}:authors")
     REDIS.ttl("auid:#{uid}").should > 13*24*60*60
   end  

   it "should set latest keys" do
     REDIS.get("latest:message").should == @data["commits"].last["message"]
     REDIS.get("latest:gravatar").should == Gravatar.url(@data["commits"].last["author"]["email"])
   end

   it "should increment total commits" do
     REDIS.get("uid:1:commits").should == "1"
   end

   it "should increment daily commits" do
     REDIS.get("uid:1:daily:#{Time.now.yday}").should == "1"
   end

   it "should increment weekly commits" do
     REDIS.get("uid:1:weekly:#{Time.now.beginning_of_week.yday}").should == "1"
   end
   
   it "should not drop volatile keys" do
     5.times {post "/hook/#{TOKEN}", :payload => payload(:author_email => "g@mikamai.com")}
     uid = REDIS.get("email:g@mikamai.com:uid")
     REDIS.get("uid:#{uid}:commits").to_i.should == 5
     REDIS.get("uid:#{uid}:daily:#{Time.now.yday}").to_i.should == 5
     REDIS.get("uid:#{uid}:weekly:#{Time.now.beginning_of_week.yday}").to_i.should == 5
   end
end