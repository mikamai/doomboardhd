describe 'Background Handling'
  describe '.getAnimationPosition'
    it "should be memoized"
      $.getAnimationPosition().should.eql $.getAnimationPosition(new Date(2010, 3, 15))
    end
    
    it "should calculate the correct position"
      $.getAnimationPosition(new Date(2010, 3, 15, 0, 0), true).should.eql 0
      $.getAnimationPosition(new Date(2010, 3, 15, 9, 0), true).should.eql 37.5
      $.getAnimationPosition(new Date(2010, 3, 15, 12, 0), true).should.eql 50
    end
  end
  
  describe '.getAnimationValues'
    it "should get the sun's correct position"
      values = [9, 13, 20, 21, 0]
      results = [0, jQuery.maximumHeight, 0, -120, -120]
      
      for (var i in values) {
        $.getAnimationPosition(new Date(2010, 3, 15, values[i]), true)
        $.getAnimationValues().height.should.eql results[i]
      }

      $.getAnimationPosition(new Date(2010, 3, 15, 20, 30), true)
      $.getAnimationValues().height.should.eql -60
    end

    it "should get the sky's correct opacity"
      values = [9, 13, 20, 21, 0]
      results = [.4, 1, .4, 0, 0]
      
      for (var i in values) {
        $.getAnimationPosition(new Date(2010, 3, 15, values[i]), true)
        $.getAnimationValues().opacity.should.eql results[i]
      }

      $.getAnimationPosition(new Date(2010, 3, 15, 20, 30), true)
      $.getAnimationValues().opacity.should.eql 0.2
    end
  end

end