require 'rubygems'
require 'dm-core'
require 'do_sqlite3'
require File.dirname(__FILE__) + '/models/project'
require File.dirname(__FILE__) + '/models/commit'

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{File.dirname(File.expand_path(__FILE__))}/../database.sqlite3")

DataMapper.auto_migrate!
