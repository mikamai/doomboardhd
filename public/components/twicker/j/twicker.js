(function($) {
  $.writeTweet = function(tweet, tw_class){
    return '<div class="tweet '+tw_class+'"><img class="avatar" src="'+tweet.profile_image_url+'"></img> <span class="username">'+tweet.from_user+'</span> '+tweet.text+'</div>';
  };
  
  $.updateTicker = function(){
    var search_url = "http://search.twitter.com/search.json?rpp=5&callback=?&q=";
    $.getJSON(search_url+searchKeyword, function(json){
      $(".search").remove();
      $.each(json.results, function(i,tweet){
           $("#tweets").append($.writeTweet(tweet, "search"));
      });
    });
    // $.getJSON("http://api.twitter.com/1/mikamai/lists/mikameisters/statuses.json", function(json){
    //   $(".mikamai").remove();
    //   $.each(json.results, function(i,tweet){
    //      $("#tweets").append(writeTweet(tweet, "mikamai"));
    //   });
    // });
  };
  $(document).ready(function(){
    $.updateTicker();
    setInterval("jQuery.updateTicker()", 60000 );
  });
  
})(jQuery)