# database - needed before requiring app
#ENV["REDIS_PORT"] = "8484"

require File.join(File.dirname(__FILE__), '..', 'app.rb')

require 'rack/test'
require 'spec'
require 'spec/autorun'
require 'spec/interop/test'
require 'nokogiri'
require 'digest/sha2'

# set test environment
set :environment, :test
set :run, false
set :raise_errors, true
set :logging, false

def encode_credentials(username, password)
  "Basic " + Base64.encode64("#{username}:#{password}")
end

def payload(options = {})
  commits = options.delete(:commits) || 1
  timestamp = options.delete(:timestamp) || Time.now.to_s
  is_private = options.delete(:private) || false
  
  before = (Digest::SHA2.new << rand(65535).to_s).to_s
  after = (Digest::SHA2.new << rand(65535).to_s).to_s
  ref = (Digest::SHA2.new << rand(65535).to_s).to_s
  url = "git@github.com:#{rand(65535)}/#{rand(65535)}.git"
  owner_email = "#{rand(65535)}@mikamai.com"
  owner_name = "#{rand(65535)}".to_s
  account = "#{rand(65535)}".to_s
  project = "#{rand(65535)}".to_s
  
  cmt = []
  
  commits.times do
    author_email = options[:author_email] || "#{rand(65535)}@mikamai.com"
    author_name = "#{rand(65535)}".to_s
    cid = (Digest::SHA2.new << rand(65535).to_s).to_s
    cmt << {
      "removed" => [],
      "url" => "",
      "modified" => "",
      "message" => "Come Foo with us.",
      "author" => {
        "email" => author_email,
        "name" => author_name
      },
      "timestamp" => timestamp,
      "id" => cid,
      "added" => []
    }
  end
  
  {
    "before" => before,
    "after" => after,
    "ref" => ref,
    "commits" => cmt,
    "repository" => {
      "owner" => {
        "email" => owner_email,
        "name" => owner_name
      },
      "pledgie" => nil,
      "url" => "git@github.com:#{account}/#{project}.git",
      "description" => "Foobar",
      "homepage" => "http://github.com/#{project}",
      "watchers" => rand(1340),
      "private" => is_private,
      "forks" => rand(1320),
      "name" => "Foobarastic"
    }
  }.to_json
end
