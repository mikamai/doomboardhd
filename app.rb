begin
  # Try to require the preresolved locked set of gems.
  require File.expand_path('../.bundle/environment', __FILE__)
rescue LoadError
  # Fall back on doing an unlocked resolve at runtime.
  require "rubygems"
  require "bundler"
  Bundler.setup
end

require 'sinatra'
require 'json'
require 'active_support'
require 'open-uri'

require File.dirname(__FILE__) + "/lib/authorization"
require File.dirname(__FILE__) + "/lib/redis-rb/lib/redis"
require File.dirname(__FILE__) + "/lib/gravatar"
require File.dirname(__FILE__) + "/config.rb"

redis_host = ENV["REDIS_HOST"] || "127.0.0.1"
redis_port = ENV["REDIS_PORT"] || 6379

REDIS = Redis.new(:host => redis_host, :port => redis_port)

helpers do
  include Sinatra::Authorization
end

get '/' do
  get_projects
  @fetch_private = false
  erb :index
end

get '/private' do
  require_administrative_privileges
  get_projects(:private => true)
  @fetch_private = true
  erb :index
end

post "/hook/#{TOKEN}" do
  payload = JSON.parse(params["payload"])  
  
  # REDIS
  pid = REDIS.get("purl:#{payload["repository"]["url"]}:pid")

  unless pid
    pid = REDIS.incr("global:nextProjectId")
    REDIS.set("purl:#{payload["repository"]["url"]}:pid", pid)
    REDIS.set("pid:#{pid}:url", payload["repository"]["url"])
    REDIS.set("pid:#{pid}:min_url", payload["repository"]["url"].split("/").last)
    REDIS.set("pid:#{pid}:name", payload["repository"]["name"])
    REDIS.set("pid:#{pid}:private", payload["repository"]["private"])
  end

  REDIS.set("apid:#{pid}", pid)
  REDIS.expire("apid:#{pid}", 14*24*60*60)
    
  payload["commits"].each do |c|
    if REDIS.sadd("pid:#{pid}:commits", c["id"])
      uid = REDIS.get("email:#{c["author"]["email"]}:uid")
      unless uid
        uid = REDIS.incr("global.nextUserId")
        REDIS.set("email:#{c["author"]["email"]}:uid", uid)
        REDIS.set("uid:#{uid}:email", c["author"]["email"])
        REDIS.set("uid:#{uid}:name", c["author"]["name"])
        REDIS.set("uid:#{uid}:gravatar", Gravatar.url(c["author"]["email"]))
      end

      REDIS.incr("uid:#{uid}:commits")
      REDIS.incr("uid:#{uid}:daily:#{Time.now.yday}")
      REDIS.incr("uid:#{uid}:weekly:#{Time.now.beginning_of_week.yday}")
      
      REDIS.set("auid:#{uid}", uid)
      REDIS.expire("auid:#{uid}", 14*24*60*60)
      REDIS.sadd("pid:#{pid}:authors", uid)
      REDIS.set("latest:message", c["message"])
      REDIS.set("latest:gravatar", Gravatar.url(c["author"]["email"]))
      REDIS.set("pid:#{pid}:latest:gravatar", Gravatar.url(c["author"]["email"]))
    end
  end
        
  "OK\n"
end

get '/projects' do
  get_projects
  erb :projects
end

get '/projects/private' do
  require_administrative_privileges
  get_projects(:private => true)
  erb :projects
end

get '/commits/latest' do
  content_type 'application/json'
  {:message => REDIS.get("latest:message"), :gravatar => REDIS.get("latest:gravatar")}.to_json
end

get '/leaderboard/commits/all-times' do
  @title = "Top all-time committers"
  @leaderboard = leaderboard("commits")
  
  erb :'leaderboard/commits'
end

get '/leaderboard/commits/today' do
  @title = "Top committers of the day"
  @leaderboard = leaderboard("daily:#{Time.now.yday}")

  erb :'leaderboard/commits'
end

get '/leaderboard/commits/weekly' do
  @title = "This week top committers"
  @leaderboard = leaderboard("weekly:#{Time.now.beginning_of_week.yday}")
  
  erb :'leaderboard/commits'
end

private
def get_projects(options = {})
  fetch_private = options.delete(:private) || false
  project_uids = REDIS.keys("apid:*").map {|key| key.split(":")[1]}
  @projects = []
  @authors = {}
  build_output = JSON.parse(Kernel.open(CONTINOUS_INTEGRATION_URL).read)
  project_statuses = {}
  build_output["projects"].each do |p|
    project_statuses[p["url"]] = p["lastBuildStatus"]
  end
  project_uids.each do |pid|
    min_url = REDIS.get("pid:#{pid}:min_url")
    name = REDIS.get("pid:#{pid}:name")
    is_private = fetch_private ? false : (REDIS.get("pid:#{pid}:private") == "true")
    @projects << {:min_url => min_url, :name => name, :build_status => project_statuses[min_url]} unless is_private
    @authors[min_url] = REDIS.smembers("pid:#{pid}:authors").map do |uid| 
      REDIS.get("uid:#{REDIS.get("auid:#{uid}")}:gravatar")
    end
    @authors[min_url].compact!
  end
  @projects = @projects.sort_by {|p| p[:name].downcase}
end

def leaderboard(timespan_key)
  l = REDIS.keys("uid:*:#{timespan_key}").map do |key|
    uid = key.split(":")[1]
    { 
      :name => REDIS.get("uid:#{uid}:name"), 
      :gravatar => REDIS.get("uid:#{uid}:gravatar"), 
      :commits => REDIS.get("uid:#{uid}:#{timespan_key}")
    }
  end
  l.sort_by {|e| -e[:commits].to_i}[0,10]
end