(function($) {
  $.maximumHeight = 700;
  $.animationPosition = -1;

  $.getAnimationPosition = function(date, force) {
    if ($.animationPosition == -1 || force) {
      if (!date) {
        date = new Date();
      }
      hours = date.getHours();
      minutes = date.getMinutes();
      $.animationPosition = (hours + minutes / 60) / 24 * 100;          
    }
    return $.animationPosition;
  };

  $.getAnimationValues = function() {
    myHeight = 0;
    myOpacity = 0;
    a = $.getAnimationPosition();
    
    if (a <= 37.5) {
      myHeight = a * 120 / 37.5 - 120;
      myOpacity = a * 0.4 / 37.5;
    } else if (a <= 54.17) {
      myHeight = (a - 37.5) * $.maximumHeight / (54.17 - 37.5);
      myOpacity = 0.4 + (a - 37.5) * 0.6 / (54.17 - 37.5);
    } else if (a <= 83.34) {
      myHeight = $.maximumHeight - (a - 54.17) * $.maximumHeight / (83.34 - 54.17);
      myOpacity = 1 - (a - 54.17) * 0.6 / (83.34 - 54.17);
    } else if (a <= 87.5) {
      myHeight = 0 - (a - 83.34) * 120 / (87.5 - 83.34);
      myOpacity = 0.4 - (a - 83.34) * 0.4 / (87.5 - 83.34);
    } else {
      myHeight = -120;
      myOpacity = 0;
    }
            
    return ({opacity: Math.round(myOpacity * 10) / 10, height: Math.round(myHeight)})
  };
  
  $.positions = function() {
    return [
      0,
      Math.round((100 - $.getAnimationPosition()) % 100),
      Math.round(((100 - $.getAnimationPosition() + 37.5) % 100)),
      Math.round(((100 - $.getAnimationPosition() + 54.17) % 100)),
      Math.round(((100 - $.getAnimationPosition() + 83.34) % 100)),
      100
    ] 
  };
  
  $.sunKeyFrames = function() {
    return [
      $.getAnimationValues().height, 
      -120, 
      0, 
      $.maximumHeight,
      0,
      Math.round($.getAnimationValues().height)
    ] 
  };
  
  $.skyKeyFrames = function() {
    return [
      $.getAnimationValues().opacity, 
      0, 
      0.4, 
      1,
      0.4, 
      $.getAnimationValues().opacity
    ] 
  };
  
  $(function() {
    positions = $.positions();
    values = $.sunKeyFrames();
    sky_values = $.skyKeyFrames();
    
    var style_string = "@-webkit-keyframes background-sun-move-dynamic {\n";
    for (var i=0; i < positions.length; i++) {
      style_string += positions[i] + "% {bottom: " + values[i] + "px}\n";      
    }
    style_string += '}';
    

    $('head').append('<style>' + style_string + '</style>');
    $("#sun").css("bottom", $.getAnimationValues().height);
    $("#sun").css("-webkit-animation-name", "background-sun-move-dynamic");
    
    style_string = "@-webkit-keyframes background-color-move-dynamic {\n";
    for (i=0; i < positions.length; i++) {
      style_string += positions[i] + "% {opacity: " + sky_values[i] + "}\n";      
    }
    style_string += '}';    
    $('head').append('<style>' + style_string + '</style>');
    $("#background_color").css("opacity", 0);    
    $("#background_color").css("-webkit-animation-name", "background-color-move-dynamic");
  });  
})(jQuery);