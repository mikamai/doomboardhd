(function($) {
  $(function() {
    var leaderboardPageIndex = 0;
    var leaderboardPages = [
      '/leaderboard/commits/all-times',
      '/leaderboard/commits/today',
      '/leaderboard/commits/weekly'
    ];
    $.updateLeaderboard = function() {
      $("#leaderboard").hide(1000, function() {
        $.get(leaderboardPages[leaderboardPageIndex], function(data) {
          $("#leaderboard").html(data);          
          leaderboardPageIndex = (leaderboardPageIndex + 1) % 3;        
          $("#leaderboard").show(1000, function() {
            window.setTimeout('jQuery.updateLeaderboard()', 20000);
          });          
        });
      })  
    }
    jQuery.updateLeaderboard();
  });
})(jQuery)