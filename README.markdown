# Doomboard

A project dashboard built for your 1080p LCD displays. 

## Features

1. Optimized for 1080p
2. Cool 8 bit graphics
3. Redis based
4. Supports Github and Integrity

## Installation

Coming soon.  